# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 15:52:29 2020

@author: MCDF
"""

import scipy.io
import numpy as np

def calculate_GDI(g):
    ''' calculate Gait deviation index (Schwartz) 
        g - kinemtatics of the patient (vector 459*9)
    '''
    
    G = scipy.io.loadmat('G.mat')
    GG = np.transpose(G['G'])
    features = scipy.io.loadmat('features.mat') #matrix of clean values
    ff = features['features']
    
    C = GG.dot(ff)
    
    for i in range(len(C)):
        if np.all(C[i,:] == np.zeros(len(C[2]))):
            CC = C[:i, :]
            break
        
    meanC = CC.mean(0)   
    
    deltaC = CC
    for i in range(len(CC)):
        deltaC[i,:] = CC[i,:]-meanC
    
    d = np.sqrt(np.sum(deltaC*deltaC,1))
    lnd = np.log(d)
    m_lnd = np.mean(lnd)
    sd_lnd = np.std(lnd)    
    Csubject = g.dot(ff)
    Cdiff = Csubject - meanC
    lnd = np.log(np.sqrt(np.sum(np.sum(Cdiff*Cdiff))))
    zlnd = (lnd-m_lnd)/(sd_lnd)    
    GDI = 100-(10*zlnd)
    
    return GDI

