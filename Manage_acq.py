import pandas as pd
from pyCGM2 import btk
import numpy as np

def acq_to_df(acq, list_angles, ev_fs, ff, xt, tn):

    df = pd.DataFrame()    
    for ang in list_angles:
        print(ang)
        angle = acq.GetPoint(ang+'Angles')
        angle = angle.GetValues()
        df[ang + '_x'] = np.interp(tn, xt, angle[ev_fs[0]-ff:ev_fs[1]-ff,0])
        df[ang + '_y'] = np.interp(tn, xt, angle[ev_fs[0]-ff:ev_fs[1]-ff,1])
        df[ang + '_z'] = np.interp(tn, xt, angle[ev_fs[0]-ff:ev_fs[1]-ff,2])


    return df