# -*- coding: utf-8 -*-
#%% 
import os
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import numpy as np
import matlab.engine
import Misplacement
import File_Processing
import plotly.express as px
import Analysis
# import meg as meg

#%% Setup Matlab engine with meg
# meg.setup('C:\\Program Files\\MATLAB\\R2021a')

#%%
app_path=os.getcwd()
result_path="C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\App\\Final Results 10mm" #TO ADAPT !
c3d_path = "C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\App\\2_DataToExport\\"
# dynamic_c3d = "01_03000_04617_20180423-GBNNN-VDEF-05.C3D"
dynamic_c3d = "03517_05339_20201211-GBNNN-VREF-06.C3D"
dynamic_c3d_sim = "03517_05339_20201211-GBNNN-VREF-06_MIS.C3D"
# static_c3d = "01_03000_04617_20180423-SBNNN-VDEF-02.C3D"
static_c3d = "03517_05339_20201211-SBNNN-VREF-01.C3D"
static_c3d_sim = "03517_05339_20201211-SBNNN-VREF-01_MIS.C3D"
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

Angle_Names=['Pelvis tilt','Pelvis obliquity','Pelvis rotation','Hip flexion/extention','Hip adduction/abduction','Hip rotation','Knee flexion/extention','Knee valgus/varus','Knee rotation','Ankle dorsi/plantar flexion','Ankle rotation','Foot progression']
Angle_values=['1','2','3','4','5','6','7','8','9','10','11','12']
Marks = {0:'0',1:'1',2:'2',3:'3',4:'4',5:'5',6:'6',7:'7',8:'8',9:'9',10:'10',11:'11',12:'12',13:'13',14:'14',15:'15',16:'16',17:'17',18:'18',19:'19',20:'20',21:'21',22:'22',23:'23',24:'24',25:'25',26:'26',27:'27',28:'28',29:'29',30:'30'}
Markers_to_plot = ['RTOE','RANK','RKNE', 'RANK', 'RTIB', 'RKNE', 'RASI', 'RKNE', 'RTHI', 'RASI','LASI', 'RASI', 'SACR', 'LASI', 'LTHI', 'LKNE', 'LASI', 'LKNE', 'LTIB', 'LANK','LKNE','LANK', 'LTOE']
curve_colors=['midnightblue','indigo','darkblue','mediumblue','blue','dodgerblue','deepskyblue','darkturquoise','mediumturquoise','mediumaquamarine']
Angles_list = ['LPelvis', 'LHip', 'LKnee', 'LAnkle', 'LFootProgress']
Marker_list = ['LASI', 'RASI', 'SACR', 'LTHI', 'LKNE', 'LTIB', 'LANK', 'LTOE']
Segment_list = ['PELVIS', 'PELVIS', 'PELVIS', 'LFEMUR', 'LFEMUR', 'LTIBIA', 'LTIBIA', 'LFOOT']
Origin_list = ['midASIS', 'midASIS', 'midASIS', 'LHJC', 'LHJC', 'LKJC', 'LKJC', 'LAJC']

sagital = ['LTHI', 'LTIB', 'LKNE', 'LANK']
coronal = ['LASI', 'RASI', 'SACR']
transversal = ['LTOE']


AP_DP=['Original','Anterior','Posterior','Distal','Proximal']
ML_DP=['Original','Medial','Lateral','Distal','Proximal']
AP_ML=['Original','Medial','Lateral','Anterior','Posterior'] 

Worst_simu=['272867','272868','272869','272870','114243','272991','272992','272993','272994','272995']
Simulation_done = 0
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

#%%
if os.path.exists(c3d_path+static_c3d_sim):
    os.remove(c3d_path+static_c3d_sim)
if os.path.exists(c3d_path+dynamic_c3d_sim):
    os.remove(c3d_path+dynamic_c3d_sim)

df1 =pd.read_csv(app_path+"\\App1 points.csv")
colnames_df2=['simu','LKNE','LTHI','LANK','LTIB','LASI','SACR','RASI','LTOE']
df2= pd.read_csv(app_path+"\\App1 combinaisons.csv", names=colnames_df2,low_memory=False)
df3=pd.read_csv(app_path+"\\App1 lines.csv")
df4=pd.read_csv(app_path+"\\App1 rms class.csv")
Original_Angle_Values=pd.read_csv(app_path+"\\p01__pyCGM1.csv", sep=';')
colnames_df5=['combi','simu','rms','angle']
df5=pd.read_csv(app_path+"\\App1 10 worst simulations (chosen angles).csv",sep=';',names=colnames_df5)

app.layout = html.Div([# FIRST ROW
    html.H2('Simulation of Conventional Gait Model markers misplacement',style={'textAlign':'center', 'backgroundColor':'black', 'color': 'white', 'fontSize' : 20}),
        
    # SECOND ROW
    html.Div(children = [
        # SECOND ROW FIRST COLUMN
        html.Label(children='DEFINE THE MISPLACEMENT TO BE ADDED TO EACH MARKER AND PRESS RUN', style = {'padding': '0px 30px', 'fontSize': 20, 'color':'red'}),
        html.Label(children='LASI (Left Anterior Iliac Spine)', style={'padding':'0px 30px', 'fontSize':19}),
        dcc.RadioItems(id='lasi',options=[{'label':i, 'value': i} for i in ML_DP],value='Original',labelStyle={'display': 'inline-block'},style={'padding':'0px 0px 20px'}),
        dcc.Slider(id='mag_lasi', min=0, max = 30, step=1, value=0, marks = Marks),

        html.Label('RASI (Right Anterior Iliac Spine)', style={'padding':'0px 30px', 'fontSize':19}),
        dcc.RadioItems(id='rasi',options=[{'label':i, 'value': i} for i in ML_DP],value='Original',labelStyle={'display': 'inline-block'},style={'padding':'0px 0px 20px'}),
        dcc.Slider(id='mag_rasi', min=0, max = 30, step=1, value=0, marks = Marks),

        html.Label('SACR (Sacrum)', style={'padding':'0px 30px', 'fontSize':19}),
        dcc.RadioItems(id='sacr',options=[{'label':i, 'value': i} for i in ML_DP],value='Original',labelStyle={'display': 'inline-block'},style={'padding':'0px 0px 20px'}),
        dcc.Slider(id='mag_sacr', min=0, max = 30, step=1, value=0, marks = Marks),

        html.Label('LTHI (Left Femoral Wand)', style={'padding':'0px 30px', 'fontSize':19}),
        dcc.RadioItems(id='lthi',options=[{'label':i, 'value': i} for i in AP_DP],value='Original',labelStyle={'display': 'inline-block'},style={'padding':'0px 0px 20px'}),
        dcc.Slider(id='mag_lthi', min=0, max = 30, step=1, value=0, marks = Marks),

        html.Label('LKNE (Left Lateral Femoral Epicondyle', style={'padding':'0px 30px', 'fontSize':19}),
        dcc.RadioItems(id='lkne',options=[{'label':i, 'value': i} for i in AP_DP],value='Original',labelStyle={'display': 'inline-block'},style={'padding':'0px 0px 20px'}),
        dcc.Slider(id='mag_lkne', min=0, max = 30, step=1, value=0, marks = Marks),

        html.Label('LTIB (Left Tibial Wand)', style={'padding':'0px 30px', 'fontSize':19}),
        dcc.RadioItems(id='ltib',options=[{'label':i, 'value': i} for i in AP_DP],value='Original',labelStyle={'display': 'inline-block'},style={'padding':'0px 0px 20px'}),
        dcc.Slider(id='mag_ltib', min=0, max = 30, step=1, value=0, marks = Marks),

        html.Label('LANK (Left Lateral Tibial Malleolus)', style={'padding':'0px 30px', 'fontSize':19}),
        dcc.RadioItems(id='lank',options=[{'label':i, 'value': i} for i in AP_DP],value='Original',labelStyle={'display': 'inline-block'},style={'padding':'0px 0px 20px'}),
        dcc.Slider(id='mag_lank', min=0, max = 30, step=1, value=0, marks = Marks),

        html.Label('LTOE (Left 2nd Metatarsal Head)', style={'padding':'0px 30px', 'fontSize':19}),
        dcc.RadioItems(id='ltoe',options=[{'label':i, 'value': i} for i in AP_ML],value='Original',labelStyle={'display': 'inline-block'},style={'padding':'0px 0px 20px'}),
        dcc.Slider(id='mag_ltoe', min=0, max = 30, step=1, value=0, marks = Marks),

        html.Label('Press RUN for initialiazation of simulation!', style={'padding':'0px 30px', 'fontsize':12}),
        html.Button('RUN', id = 'run_button', n_clicks=0),     
        html.Label('--------------------', id='output'),
        dcc.Store(id= 'intermediate_value')
    ], style = {'width': '50%', 'display': 'inline-block', 'vertical-align': 'top', 'margin-left':'3vw', 'margin-top': '2vw'}),

    # SECOND ROW SECOND COLUMN
    html.Div( children = [
        html.Button('Plot', id = 'plot_3d'),
        dcc.Graph(id='body',style={'height':1100, 'width':1100, 'fontSize':15})], style={'width': '20%','display':'inline-block', 'vertical-align':'top', 'margin-right':'3vw', 'margin-top':'3vw'}),

    # THIRD ROW
    html.Div(children = [
        dcc.Graph(id= 'Kinematics_plots')], style={'height':100000,'width': '90%','display':'inline-block', 'vertical-align':'top', 'margin-right':'3vw', 'margin-top':'3vw', 'padding': '60px 30px 30px 0px'})
])

# ADD MISPLACEMENT + COMPUTE KINEMATICS (MOVECK) + UPDATE GRAPHS
@app.callback([dash.dependencies.Output('Kinematics_plots', 'figure'),
        dash.dependencies.Output('body', 'figure'),
        dash.dependencies.Output('run_button', 'n_clicks')],    
        
        [dash.dependencies.Input('run_button', 'n_clicks'),
        dash.dependencies.Input('lasi', 'value'),
        dash.dependencies.Input('rasi', 'value'),
        dash.dependencies.Input('sacr', 'value'),
        dash.dependencies.Input('lthi', 'value'),
        dash.dependencies.Input('lkne', 'value'),
        dash.dependencies.Input('ltib', 'value'),
        dash.dependencies.Input('lank', 'value'),
        dash.dependencies.Input('ltoe', 'value'),
        dash.dependencies.Input('mag_lasi', 'value'),
        dash.dependencies.Input('mag_rasi', 'value'),
        dash.dependencies.Input('mag_sacr', 'value'),
        dash.dependencies.Input('mag_lthi', 'value'),
        dash.dependencies.Input('mag_lkne', 'value'),
        dash.dependencies.Input('mag_ltib', 'value'),
        dash.dependencies.Input('mag_lank', 'value'),
        dash.dependencies.Input('mag_ltoe', 'value')]) 

def Marker_Misplacement(n_clicks, lasi, rasi, sacr, lthi, lkne,ltib,lank,ltoe,mag_lasi,mag_rasi,mag_sacr, mag_lthi, mag_lkne, mag_ltib, mag_lank, mag_ltoe):
    if os.path.exists(c3d_path+static_c3d_sim):
        os.remove(c3d_path+static_c3d_sim)
    if os.path.exists(c3d_path+dynamic_c3d_sim):
        os.remove(c3d_path+dynamic_c3d_sim)

    if n_clicks > 0: 
        Mag_list = [mag_lasi, mag_rasi, mag_sacr, mag_lthi, mag_lkne, mag_ltib, mag_lank, mag_ltoe]
        Angle_list = [lasi, rasi, sacr, lthi, lkne, ltib, lank, ltoe]
        
        dyn_mis_filename = dynamic_c3d.replace('.C3D', '_MIS.C3D')
        stat_mis_filename = static_c3d.replace('.C3D', '_MIS.C3D')
        
        #Create a copy of the original files
        acq_dyn = File_Processing.reader(c3d_path+dynamic_c3d)
        acq_stat = File_Processing.reader(c3d_path+static_c3d)
        File_Processing.writer(acq_dyn,c3d_path, dyn_mis_filename)
        File_Processing.writer(acq_stat, c3d_path, stat_mis_filename)
        print('[INFO] 1. Sim c3d copy created with suffix _MIS')


            # add misp and update c3d
        Misplacement.move(c3d_path, dyn_mis_filename, Marker_list, Mag_list, Angle_list, Segment_list, Origin_list, sagital, coronal, transversal)
        Misplacement.move(c3d_path, stat_mis_filename,  Marker_list, Mag_list, Angle_list, Segment_list, Origin_list, sagital, coronal, transversal)
        print("[INFO] 3. Added misplacement on the markers")

        # Run moveck
        Eng = matlab.engine.start_matlab()
        a = Eng.Moveck_run(c3d_path+stat_mis_filename, c3d_path+dyn_mis_filename) #test without output. Just writting kinematics in the c3d.
        print('[INFO] 4. Angles computed with moveck')
        # b = Eng.Moveck_run(c3d_path+static_c3d, c3d_path+dynamic_c3d)
        tn = np.linspace(0,100,101)

        acq_sim = File_Processing.reader(c3d_path+dynamic_c3d_sim)
        acq_ori = File_Processing.reader(c3d_path+dynamic_c3d)
        Ev = acq_sim.GetEvents()
        ff = acq_sim.GetFirstFrame()
        metadata = acq_sim.GetMetaData()
        lfo = []
        lfs = []
        rfo = []
        rfs = []

        try:
            list(map(int, metadata.FindChild('CYCLES').value().FindChild('Left_cycle').value().GetInfo().ToString()[0].split(',')))
        except:
            print('[WARNING] No Cycle information found on metadata. Cant extract data')      
        else:
            LC = list(map(int, metadata.FindChild('CYCLES').value().FindChild('Left_cycle').value().GetInfo().ToString()[0].split(',')))
            RC = list(map(int, metadata.FindChild('CYCLES').value().FindChild('Right_cycle').value().GetInfo().ToString()[0].split(',')))

            for ev in range(Ev.GetItemNumber()):
                event = Ev.GetItem(ev)
                if event.GetLabel() == 'Foot Strike' and event.GetContext() == 'Left':
                    lfs.append(event.GetFrame())
                elif event.GetLabel() == 'Foot Off' and event.GetContext() == 'Left':
                    lfo.append(event.GetFrame())
                elif event.GetLabel() == 'Foot Strike' and event.GetContext() == 'Right':
                    rfs.append(event.GetFrame())
                elif event.GetLabel() == 'Foot Off' and event.GetContext() == 'Right':
                    rfo.append(event.GetFrame())
                else:
                    print('[WARNING] Unindentified event')
                
            # Delete first foot off if preceed foot strike
            if len(lfs)> 0 and len(lfo) and lfs[0]>lfo[0]:
                lfo.pop(0)
            if len(rfs)>0 and len(rfo)>0 and rfs[0]>rfo[0]:
                rfo.pop(0)
            # Delete last foot off if after foot strike
            if len(lfs)>0 and len(lfo)>0 and lfs[-1]<lfo[-1]:
                lfo.pop(-1)
            if len(rfs)>0 and len(rfo)>0 and rfs[-1]<rfo[-1]:
                rfo.pop(-1)

        fig = make_subplots(rows=4, cols = 3, start_cell='bottom-left', subplot_titles=('Ankle Flexion', 'Ankle Rotation', 'Foot Progression','Knee Flexion', 'Knee Var/Valgus', 'Knee Rotation', 'Hip Flexion', 'Hip Add/Abd', 'Hip Rotation','Pelvis Obliquity','Pelvis Tilt', 'Pelvis Rotation'))
        angle_sim = acq_sim.GetPoint('LPelvisAngles').GetValues()
        # angle = angle.GetValues()
        angle_ori = acq_ori.GetPoint('LPelvisAngles').GetValues()
        # angle_ori = angle_ori.GetValues()

        xt = np.linspace(0, 101, len(angle_sim[lfs[0]-ff:lfs[1]-ff,0]))
        for i in range(3):
            P_norm_sim = np.interp(tn, xt, angle_sim[lfs[0]-ff:lfs[1]-ff,i])
            P_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,i])
            rms = Analysis.rms(P_norm_ori, P_norm_sim)
            if i == 1:
                fig.add_trace(go.Scatter(x = tn, y = P_norm_ori, mode='lines', line_color='#0000ff', name='Original'), row=4, col = i+1)
                fig.append_trace(go.Scatter(x = tn, y = P_norm_sim, mode='lines', line_color='#d62728', name = 'Simulated'), row=4, col = i+1)
                fig.append_trace(go.Scatter(x=[90],y=[20],mode="lines+text",text=["RMS: "+str(round(rms,1))],textposition="bottom center", showlegend = False), row=4, col = i+1)
            else:
                fig.add_trace(go.Scatter(x = tn, y = P_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=4, col = i+1)
                fig.append_trace(go.Scatter(x = tn, y = P_norm_sim, mode='lines', line_color='#d62728', showlegend = False), row=4, col = i+1)
                fig.append_trace(go.Scatter(x=[90],y=[20],mode="lines+text",text=["RMS: "+str(round(rms,1))],textposition="bottom center", showlegend = False), row=4, col = i+1)

        angle_sim = acq_sim.GetPoint('LHipAngles').GetValues()
        angle_ori = acq_ori.GetPoint('LHipAngles').GetValues()
        max_plot = [60, 20, 30]
        for i in range(3):
            P_norm_sim = np.interp(tn, xt, angle_sim[lfs[0]-ff:lfs[1]-ff,i])
            P_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,i])
            rms = Analysis.rms(P_norm_ori, P_norm_sim)
            fig.add_trace(go.Scatter(x = tn, y = P_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=3, col = i+1)
            fig.append_trace(go.Scatter(x = tn, y = P_norm_sim, mode='lines', line_color='#d62728', showlegend = False), row=3, col = i+1)
            fig.append_trace(go.Scatter(x=[90],y=[max_plot[i]],mode="lines+text",text=["RMS: "+str(round(rms,1))],textposition="bottom center", showlegend = False), row=3, col = i+1)
        
        angle_sim = acq_sim.GetPoint('LKneeAngles').GetValues()
        angle_ori = acq_ori.GetPoint('LKneeAngles').GetValues()
        max_plot = [80, 30, 20]
        for i in range(3):
            K_norm_sim = np.interp(tn, xt, angle_sim[lfs[0]-ff:lfs[1]-ff,i])
            K_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,i])
            rms = Analysis.rms(K_norm_ori, K_norm_sim)
            fig.add_trace(go.Scatter(x = tn, y = K_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=2, col = i+1)
            fig.append_trace(go.Scatter(x = tn, y = K_norm_sim, mode='lines', line_color='#d62728', showlegend = False), row=2, col = i+1)
            fig.append_trace(go.Scatter(x=[90],y=[max_plot[i]],mode="lines+text",text=["RMS: "+str(round(rms,1))],textposition="bottom center", showlegend = False), row=2, col = i+1)
        
        angle_sim = acq_sim.GetPoint('LAnkleAngles').GetValues()
        angle_ori = acq_ori.GetPoint('LAnkleAngles').GetValues()
        AX_norm_sim = np.interp(tn, xt, angle_sim[lfs[0]-ff:lfs[1]-ff,0])
        AX_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,0])
        rms = Analysis.rms(AX_norm_ori, AX_norm_sim)
        fig.add_trace(go.Scatter(x = tn, y = AX_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=1, col = 1)
        fig.append_trace(go.Scatter(x = tn, y = AX_norm_sim, mode='lines', line_color='#d62728', showlegend = False), row=1, col = 1)
        fig.append_trace(go.Scatter(x=[90],y=[20],mode="lines+text",text=["RMS: "+str(round(rms,1))],textposition="bottom center", showlegend = False), row=1, col = 1)

        AZ_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,2])
        AZ_norm_sim = np.interp(tn, xt, angle_sim[lfs[0]-ff:lfs[1]-ff,2])
        rms = Analysis.rms(AZ_norm_ori, AZ_norm_sim)
        fig.add_trace(go.Scatter(x = tn, y = AZ_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=1, col = 2)
        fig.append_trace(go.Scatter(x = tn, y = AZ_norm_sim, mode='lines', line_color='#d62728', showlegend = False), row=1, col = 2)
        fig.append_trace(go.Scatter(x=[90],y=[20],mode="lines+text",text=["RMS: "+str(round(rms,1))],textposition="bottom center", showlegend = False), row=1, col = 2)

        angle_sim = acq_sim.GetPoint('LFootProgressAngles').GetValues()
        angle_ori = acq_ori.GetPoint('LFootProgressAngles').GetValues()
        
        FZ_norm_sim = np.interp(tn, xt, angle_sim[lfs[0]-ff:lfs[1]-ff,2])
        FZ_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,2])
        rms = Analysis.rms(FZ_norm_sim, FZ_norm_ori)
        fig.add_trace(go.Scatter(x = tn, y = FZ_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=1, col = 3)
        fig.append_trace(go.Scatter(x = tn, y = FZ_norm_sim, mode='lines', line_color='#d62728', showlegend = False), row=1, col = 3)
        fig.append_trace(go.Scatter(x=[90],y=[20],mode="lines+text",text=["RMS: "+str(round(rms,1))],textposition="bottom center", showlegend = False), row=1, col = 3)

        fig.update_layout(autosize=False,width=1800,height=900,)
        ## UPDATE 3D SCATTER
        marker_c_x = []
        marker_c_y = []
        marker_c_z = []
        
        acq_stat_ori = File_Processing.reader(c3d_path+static_c3d)
        for marker in Markers_to_plot:
            marker_c_x.append( acq_stat_ori.GetPoint(marker).GetValues()[10,0])
            marker_c_y.append( acq_stat_ori.GetPoint(marker).GetValues()[10,1])
            marker_c_z.append( acq_stat_ori.GetPoint(marker).GetValues()[10,2])

        Markers = {'X':marker_c_x, 'Y':marker_c_y, 'Z':marker_c_z, 'marker_label': Markers_to_plot}
        df = pd.DataFrame(Markers)
        trace = [go.Scatter3d(x=df['X'], y=df['Y'], z=df['Z'], mode = 'lines+markers+text', name = "Original", text=df['marker_label'])]

        if os.path.isfile(c3d_path+dynamic_c3d_sim):
            marker_c_x = []
            marker_c_y = []
            marker_c_z = []

            acq_stat_sim = File_Processing.reader(c3d_path+static_c3d_sim)
            for marker in Markers_to_plot:
                marker_c_x.append( acq_stat_sim.GetPoint(marker).GetValues()[10,0])
                marker_c_y.append( acq_stat_sim.GetPoint(marker).GetValues()[10,1])
                marker_c_z.append( acq_stat_sim.GetPoint(marker).GetValues()[10,2])

            Markers = {'X':marker_c_x, 'Y':marker_c_y, 'Z':marker_c_z}
            df = pd.DataFrame(Markers)
            trace_mis = [go.Scatter3d(x=df['X'], y=df['Y'], z=df['Z'], mode = 'lines+markers', name = "Simulated")]
            fig3d = go.Figure(data = trace+trace_mis)
            fig3d.update_layout(scene = dict(xaxis = dict(nticks=4, range=[-500,800]), yaxis = dict(nticks=4, range=[-500,800]), zaxis = dict(nticks=4, range=[-0,1300])),width=700, margin=dict(r=20, l=10, b=10, t=10))

        n_clicks = 0
        return[fig, fig3d, n_clicks]
    else:
        tn = np.linspace(0,100,101)

        acq_ori = File_Processing.reader(c3d_path+dynamic_c3d)
        Ev = acq_ori.GetEvents()
        ff = acq_ori.GetFirstFrame()
        metadata = acq_ori.GetMetaData()
        lfo = []
        lfs = []

        try:
            list(map(int, metadata.FindChild('CYCLES').value().FindChild('Left_cycle').value().GetInfo().ToString()[0].split(',')))
        except:
            print('[WARNING] No Cycle information found on metadata. Cant extract data')      
        else:
            LC = list(map(int, metadata.FindChild('CYCLES').value().FindChild('Left_cycle').value().GetInfo().ToString()[0].split(',')))
            RC = list(map(int, metadata.FindChild('CYCLES').value().FindChild('Right_cycle').value().GetInfo().ToString()[0].split(',')))

            for ev in range(Ev.GetItemNumber()):
                event = Ev.GetItem(ev)
                if event.GetLabel() == 'Foot Strike' and event.GetContext() == 'Left':
                    lfs.append(event.GetFrame())
                elif event.GetLabel() == 'Foot Off' and event.GetContext() == 'Left':
                    lfo.append(event.GetFrame())
                else:
                    print('[WARNING] Unindentified event')
                
            # Delete first foot off if preceed foot strike
            if len(lfs)> 0 and len(lfo) and lfs[0]>lfo[0]:
                lfo.pop(0)
            # Delete last foot off if after foot strike
            if len(lfs)>0 and len(lfo)>0 and lfs[-1]<lfo[-1]:
                lfo.pop(-1)

        fig = make_subplots(rows=4, cols = 3, start_cell='bottom-left', subplot_titles=('Ankle Flexion', 'Ankle Rotation', 'Foot Progression','Knee Flexion', 'Knee Var/Valgus', 'Knee Rotation', 'Hip Flexion', 'Hip Add/Abd', 'Hip Rotation','Pelvis Obliquity','Pelvis Tilt', 'Pelvis Rotation'))
        angle_ori = acq_ori.GetPoint('LHipAngles').GetValues()

        xt = np.linspace(0, 101, len(angle_ori[lfs[0]-ff:lfs[1]-ff,0]))
        for i in range(3):
            P_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,i])
            if i == 1:
                fig.add_trace(go.Scatter(x = tn, y = P_norm_ori, mode='lines', line_color='#0000ff', name='Original'), row=4, col = i+1)
            else:
                fig.add_trace(go.Scatter(x = tn, y = P_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=4, col = i+1)

        angle_ori = acq_ori.GetPoint('LHipAngles').GetValues()
        for i in range(3):
            P_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,i])
            fig.add_trace(go.Scatter(x = tn, y = P_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=3, col = i+1)

        angle_ori = acq_ori.GetPoint('LKneeAngles').GetValues()
        for i in range(3):
            P_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,i])
            fig.add_trace(go.Scatter(x = tn, y = P_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=2, col = i+1)

        angle_ori = acq_ori.GetPoint('LAnkleAngles').GetValues()
        AX_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,0])
        fig.add_trace(go.Scatter(x = tn, y = AX_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=1, col = 1)
        AZ_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,2])
        fig.add_trace(go.Scatter(x = tn, y = AZ_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=1, col = 2)

        angle_ori = acq_ori.GetPoint('LFootProgressAngles').GetValues()
        FZ_norm_ori = np.interp(tn, xt, angle_ori[lfs[0]-ff:lfs[1]-ff,2])
        fig.add_trace(go.Scatter(x = tn, y = FZ_norm_ori, mode='lines', line_color='#0000ff', showlegend = False), row=1, col = 3)

        fig.update_layout(autosize=False,width=1800,height=900,)

        ## UPDATE 3D SCATTER
        marker_c_x = []
        marker_c_y = []
        marker_c_z = []
        
        acq_stat_ori = File_Processing.reader(c3d_path+static_c3d)
        for marker in Markers_to_plot:
            marker_c_x.append( acq_stat_ori.GetPoint(marker).GetValues()[10,0])
            marker_c_y.append( acq_stat_ori.GetPoint(marker).GetValues()[10,1])
            marker_c_z.append( acq_stat_ori.GetPoint(marker).GetValues()[10,2])

        Markers = {'X':marker_c_x, 'Y':marker_c_y, 'Z':marker_c_z, 'marker_label': Markers_to_plot}
        df = pd.DataFrame(Markers)
        trace = [go.Scatter3d(x=df['X'], y=df['Y'], z=df['Z'], mode = 'lines+markers+text', name = "Original", text=df['marker_label'])]
        fig3d = go.Figure(data=trace).update_yaxes(scaleanchor = 'x', scaleratio=0)
        fig3d.update_layout(scene = dict(xaxis = dict(nticks=4, range=[-500,800]), yaxis = dict(nticks=4, range=[-500,800]), zaxis = dict(nticks=4, range=[-0,1300])),width=700, margin=dict(r=20, l=10, b=10, t=10))

        return[fig, fig3d, n_clicks]


if __name__ == '__main__': 
    app.run_server(debug=True)
else:
    print('Error')


