# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 12:59:41 2020

@author: MCDF
"""

import scipy.io
import numpy as np

def calculate_GPS(Data_patient):
    ''' calculate GPS 
        g - kinemtatics of the patient (vector 459*9)
        warning: only left leg considered 
    '''
   
#    G = scipy.io.loadmat('G.mat')
#    GG = np.transpose(G['G'])
#    features = scipy.io.loadmat('features.mat') #matrix of clean values
#    ff = features['features']
    Gmean = scipy.io.loadmat('Gmean')
    G_m = Gmean['G']
    G_mean = np.transpose(G_m.reshape(9,51))
#    # prepare patient data [matrix of 15  columns PT,2HF,2KF,2AF,PO,2HA,PR,2HR,2FP] interp to 51 points
#    data_f = data[1:,1:13]
#    data_ff = data_f.astype(np.float)
#    tn = np.linspace(0,50,51) # interpolate into 51 points
#    LPelvisAngles_interp = np.zeros((51,3))
#    LHipAngles_interp = np.zeros((51,3))
#    LKneeAngles_interp = np.zeros((51,1))
#    LAnkleAngles_interp = np.zeros((51,1))
#    LFootProgAngles_interp = np.zeros((51,1))
#    LGC1t = np.linspace(0, 50, len(data[1:,1:4]))
#    for i in range(3):
#        LPelvisAngles_interp[:,i] = np.interp(tn, LGC1t, data_ff[:,i])
#        LHipAngles_interp[:,i] = np.interp(tn, LGC1t, data_ff[:,i+3])
#    LKneeAngles_interp[:,0] = np.interp(tn, LGC1t, data_ff[:,6])
#    LAnkleAngles_interp[:,0] = np.interp(tn, LGC1t, data_ff[:,9])
#    LFootProgAngles_interp[:,0] = np.interp(tn, LGC1t, data_ff[:,11])
#    
#    Data_patient = np.transpose(np.array([LPelvisAngles_interp[:,0], LHipAngles_interp[:,0], LKneeAngles_interp[:,0], \
#                    LAnkleAngles_interp[:,0], LPelvisAngles_interp[:,1], LHipAngles_interp[:,1],\
#                    LPelvisAngles_interp[:,2],  LHipAngles_interp[:,2], LFootProgAngles_interp[:,0]]))

    # prepare control data [matrix of 15  columns PT,HF,KF,AF,PO,HA,PR,HR,FP] 
    Data_control = np.transpose(np.array([G_mean[:,0], G_mean[:,3], G_mean[:,6], G_mean[:,7], G_mean[:,1], \
                             G_mean[:,4], G_mean[:,2], G_mean[:,5], G_mean[:,8]]))
    
#    fig, axs = plt.subplots(3,3)
#    axs[0,0].plot(LPelvisAngles_interp[:,0], 'r')
#    axs[0,0].plot(Data_Control[:,0], 'b')
#    axs[0,1].plot(LPelvisAngles_interp[:,1], 'r')
#    axs[0,1].plot(Data_Control[:,1], 'b')
#    axs[0,2].plot(LPelvisAngles_interp[:,2], 'r')
#    axs[0,2].plot(Data_Control[:,2], 'b')
#    axs[1,0].plot( LHipAngles_interp[:,0],'r')
#    axs[1,0].plot(Data_Control[:,3], 'b')
#    axs[1,1].plot( LHipAngles_interp[:,1],'r')
#    axs[1,1].plot(Data_Control[:,4], 'b')
#    axs[1,2].plot( LHipAngles_interp[:,2],'r')
#    axs[1,2].plot(Data_Control[:,5], 'b')
#    axs[2,0].plot( LKneeAngles_interp[:,0],'r')
#    axs[2,0].plot(Data_Control[:,6], 'b')
#    axs[2,1].plot( LAnkleAngles_interp[:,0],'r') 
#    axs[2,1].plot(Data_Control[:,7], 'b')
#    axs[2,2].plot( LFootProgAngles_interp[:,0],'r')
#    axs[2,2].plot(Data_Control[:,8], 'b')
#    
    

    # Calculation GPS MAP for left leg (consider only left cycle)
    MAP = np.zeros([1,len(G_mean[1])+2])
    for n in range(0,len(G_mean[1])):
        MAP[:,n] = np.sqrt(np.mean( (Data_control[:,n] - Data_patient[:,n])**2) )
        
     # MAP 15, 16, 17 are Left, Right & Overall GPS
    MAP[:,9] = np.sqrt( (MAP[:,0]**2+MAP[:,2]**2+MAP[:,1]**2+MAP[:,3]**2+MAP[:,4]**2+MAP[:,5]**2+MAP[:,6]**2+MAP[:,7]**2+MAP[:,8]**2)/9 ) 
    MAP[:,10] = np.sqrt(np.mean(MAP[:,0:8]**2))

#    MAP[15,t] = np.sqrt( (MAP[0,t]**2+MAP[1,t]**2+MAP[3,t]**2+MAP[5,t]**2+MAP[7,t]**2+MAP[8,t]**2+MAP[10,t]**2+MAP[11,t]**2+MAP[13,t]**2)/9 ) 
#    MAP[16,t] = np.sqrt( (MAP[0,t]**2+MAP[2,t]**2+MAP[4,t]**2+MAP[6,t]**2+MAP[7,t]**2+MAP[9,t]**2+MAP[10,t]**2+MAP[12,t]**2+MAP[14,t]**2)/9 ) 
#    MAP[17,t] = np.sqrt(np.mean(MAP[0:15,t]**2))
    return MAP