#%% 
import numpy as np
import pandas as pd
from pyCGM2 import btk
import Simplify_acq
import File_Processing

#%%
Data_path = 'C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\App\\1_Data\\'
DataToExport_path = 'C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\App\\2_DataToExport\\'
ExportCsv_path = 'C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\App\\3_InputData\\'


Markers_To_Keep = ['LTOE','LHEE','LMET','LMED','LANK','LTIB',\
                   'RTOE','RHEE','RMET','RMED','RANK','RTIB','RTTU','RKNE','RTHI',\
                   'LTTU','LKNE','LTHI','LASI','RASI','LPSI','RPSI']
Metadata_To_Export = ["Bodymass","Height","LeftLegLength","RightLegLength","LeftKneeWidth","RightKneeWidth","LeftAnkleWidth","RightAnkleWidth","LeftSoleDelta","RightSoleDelta","LeftShoulderOffset","LeftElbowWidth","LeftWristWidth","LeftHandThickness","RightShoulderOffset","RightElbowWidth","RightWristWidth","RightHandThickness"]

#%%
# static_file = '01_03000_04617_20180423-SBNNN-VDEF-02.C3D'
# dynamic_file = '01_03000_04617_20180423-GBNNN-VDEF-05.C3D'
static_file = '03517_05339_20201211-SBNNN-VREF-01.C3D'
dynamic_file = '03517_05339_20201211-GBNNN-VREF-06.C3D'

#%% Export kinematics
Angles_to_Export = ['LPelvisAngles_pyCGM2_1', 'RPelvisAngles_pyCGM2_1', 'LHipAngles_pyCGM2_1', 'RHipAngles_pyCGM2_1', 'LKneeAngles_pyCGM2_1', 'RKneeAngles_pyCGM2_1', 'LAnkleAngles_pyCGM2_1', 'RAnkleAngles_pyCGM2_1', 'LFootProgressAngles_pyCGM2_1', 'RFootProgressAngles_pyCGM2_1']
ptk = np.concatenate((Markers_To_Keep, Angles_to_Export))

#%%
Simplify_acq.new_dyn(dynamic_file, ptk, Data_path, DataToExport_path)
acq_dyn = File_Processing.reader(DataToExport_path + dynamic_file)
acq_sta = File_Processing.reader(Data_path + static_file)
# %%
MarkersDyn_df = pd.DataFrame()
MarkersSta_df = pd.DataFrame()

for marker in Markers_To_Keep:
    m = acq_dyn.GetPoint(marker).GetValues()
    MarkersDyn_df[marker+'_X'] = m[:,0]
    MarkersDyn_df[marker+'_Y'] = m[:,1]
    MarkersDyn_df[marker+'_Z'] = m[:,2]

    m = acq_sta.GetPoint(marker).GetValues()
    MarkersSta_df[marker+'_X'] = m[:,0]
    MarkersSta_df[marker+'_Y'] = m[:,1]
    MarkersSta_df[marker+'_Z'] = m[:,2]

Angles_df = pd.DataFrame()
for angles in Angles_to_Export:
    m = acq_dyn.GetPoint(angles).GetValues()
    Angles_df[angles+'_X'] = m[:,0]
    Angles_df[angles+'_Y'] = m[:,1]
    Angles_df[angles+'_Z'] = m[:,2]
# %% export markers to csv
MarkersDyn_df.to_csv(ExportCsv_path+dynamic_file[:-4] + '.csv')
MarkersSta_df.to_csv(ExportCsv_path+static_file[:-4] + '.csv')
Angles_df.to_csv(ExportCsv_path+dynamic_file[:-4]+'_kinematics.csv')

# %%
metadata = acq_sta.GetMetadata()
Metadata = {}
Metadata["Bodymass"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[0]
Metadata["Height"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[1]
Metadata["LeftLegLength"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[4]
Metadata["RightLegLength"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[7]
Metadata["LeftKneeWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[3]
Metadata["RightKneeWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[6]
Metadata["LeftAnkleWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[2]
Metadata["RightAnkleWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[5]
