import numpy as np
import Rotation_Matrix
import Definition
import File_Processing
from pyCGM2 import btk


def move(c3d_path, c3d_filename, Marker_list, Mag_list, Angle_list, Segment_list, Origin_list, sagital, coronal, transversal):
    # seg: segment name (e.g. PELVIS, LFEMUR) - <string>
    # ori: origin marker (e.g. LHJC, LKJC) - <string>
    # marker: marker name (e.g. LASI, LANK) - <string>
    # ang: misplacement angle (from 0 to 360) - <int>
    # mag: magnitude misplacement - <int>
    # plane: plane code of misplacement (options: ML_AP for transversal, ML_DP for sagital, AP_DP for coronal) - <string>

    # SEGMENT_origin=Definition.definition_marker(current_path, method)[0][m]
    # Error_dir=Definition.definition_marker(current_path, method)[1][m]
    # SEGMENT_name=Definition.definition_marker(current_path, method)[2][m]
    acq = File_Processing.reader(c3d_path+c3d_filename)
    for i in range(len(Marker_list)):
        marker =  Marker_list[i]
        mag    =  Mag_list[i]
        angle  =  Angle_list[i]
        seg    =  Segment_list[i]
        ori    =  Origin_list[i]
        
        # LASI correction
        if marker == 'LASI' and angle == 'Medial':
            angle = 'Lateral'
        elif marker  == 'LASI' and angle == 'Lateral':
            angle = 'Medial'

        if angle == 'Original':
            mag = 0
            ang = 0
        if marker in sagital:
            plane = 'AP_DP'
            if angle == 'Anterior':
                ang = 0
            elif angle == 'Proximal':
                ang = 90
            elif angle == 'Posterior':
                ang = 180
            elif angle == 'Distal':
                ang = 270

        elif marker in coronal:
            plane = 'ML_DP'
            if angle == 'Medial':
                ang = 0
            elif angle == 'Proximal':
                ang = 90
            elif angle == 'Lateral':
                ang = 180
            elif angle == 'Distal':
                ang = 270

        elif marker in transversal:
            plane = 'AP_ML'
            if angle == 'Medial':
                ang = 0
            elif angle == 'Anterior':
                ang = 90
            elif angle == 'Lateral':
                ang = 180
            elif angle == 'Posterior':
                ang = 270
        if angle != 'Original':
            Segment_proximal = seg + '_Z'
            Segment_lateral  = seg + '_Y'
            Segment_anterior = seg + '_X'

            # acq = File_Processing.reader(c3d_path+c3d_filename)
            mark_MIS=np.zeros((acq.GetPointFrameNumber(),3))
            mark_GCS = acq.GetPoint(marker).GetValues()
            Origin_GCS = acq.GetPoint(ori).GetValues()
            Proximal_GCS = acq.GetPoint(Segment_proximal).GetValues()
            Lateral_GCS = acq.GetPoint(Segment_lateral).GetValues()
            Anterior_GCS = acq.GetPoint(Segment_anterior).GetValues()
            Error = Definition.definition_error(ang, mag, plane)

            for n in range(0,acq.GetPointFrameNumber()): 
                #Rotation matrix from GCS to LCS (3x3) -> Transformation matrix (4x4)
                Ori=np.array(Origin_GCS[n,:],dtype="float")
                Rot=Rotation_Matrix.rotation_mat(Origin_GCS,Proximal_GCS,Lateral_GCS,Anterior_GCS,n)[:,0]
                mat_left_fem=np.concatenate((Rot,Ori[:,None]),axis=1)
                T = np.concatenate((mat_left_fem,np.array([0,0,0,1])[None,:]),axis=0)
                Transf=np.linalg.inv(T)
                #Calculate marker coordinates in LCS
                mark_LCS = np.dot(Transf,np.append(mark_GCS[n,:],1))
                #Add an error on the marker in LCS
                mark_MIS_LCS=[mark_LCS[0]+Error[0],mark_LCS[1]+Error[1],mark_LCS[2]+Error[2],1]
                #Calculate marker coordinates in GCS
                mark_MIS_GCS=np.dot(np.linalg.inv(Transf),np.transpose(mark_MIS_LCS))
                #add new coordinates to point
                mark_MIS[n,:]=mark_MIS_GCS[:3]

            acq.GetPoint(marker).SetValues(mark_MIS)
    # writer = btk.btkAcquisitionFileWriter()
    # writer.SetInput(acq)
    # writer.SetFilename(c3d_path + c3d_filename)
    # writer.Update()  
    File_Processing.writer(acq, c3d_path, c3d_filename)