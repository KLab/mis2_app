function a = Moveck_run(Stat_files,Dyn_files)
% Moveck_run (adaptation of Mickael November 2021)
% Code allowing to compute the CGM from Moveck and store the results in the 
% corresponding c3d files processed
% Stat_files = 'C:\Users\FONSECMI\OneDrive - unige.ch\Projects\MIS\App\2_DataToExport\03517_05339_20201211-SBNNN-VREF-01_MIS.C3D';
% Dyn_files = 'C:\Users\FONSECMI\OneDrive - unige.ch\Projects\MIS\App\2_DataToExport\03517_05339_20201211-GBNNN-VREF-06_MIS.C3D';
a = 1;
% (MFC) declaration of the global variable error_file_idx which will
% contain the indexes of the files producing an error
global error_file_idx
error_file_idx = cell(1, 2);
% (MFC) declaration of the global variable warning_msg which will contain 
% the static files producing a warning because of missing calibration 
% settings and the corresponding warning messages
global warning_msg
warning_msg = {};
% (MFC) declaration of the global variable calibration_settings which will
% contain the static files and their corresponding calibration settings
global calibration_settings
calibration_settings = {};

% (MFC) Param structure initialization
Param.compute_markerFiltering = 0;
Param.compute_markerFillgap = 0;
Param.exportC3D_markers = 0;
Param.exportC3D_angles = 1;
Param.exportC3D_forces = 0;
Param.exportC3D_moments = 0;
Param.exportC3D_powers = 0;
Param.exportC3D_jointCenters = 0;
Param.markerFillgapMaxGapLength = 0.1;
Param.markerFilterOrder = 4;
Param.markerFilterCutoffFrequency = 6;

% (MFC) importation of the DefaultCalibrationParameters.txt and 
% Mapping.txt files
Param.defaultCalibration = readcell('DefaultCalibrationParameters.txt', 'Delimiter', '=', 'CommentStyle', '/*');
Mappings = readcell('Mappings.txt', 'Delimiter', {'{', '}'}, 'LineEnding',{'};'});
for i = 1:size(Mappings, 1)
    if contains(Mappings{i, 1}, '/*')
        if contains(Mappings{i, 1}, 'marker', 'IgnoreCase', true)
            Param.markersMapping = regexprep(regexp(Mappings{i, 2}, ',', 'split'), '\W', '');
        elseif contains(Mappings{i, 1}, 'kinematic', 'IgnoreCase', true)
            Param.kinematicsMapping = regexprep(regexp(Mappings{i, 2}, ',', 'split'), '\W', '');
        elseif contains(Mappings{i, 1}, 'kinetic', 'IgnoreCase', true)
            Param.kineticsMapping = regexprep(regexp(Mappings{i, 2}, ',', 'split'), '\W', '');
        elseif contains(Mappings{i, 1}, 'center', 'IgnoreCase', true)
            Param.jointCentersMapping = regexprep(regexp(Mappings{i, 2}, ',', 'split'), '\W', '');
        end
    end
end

passekinetic = 0;
passekinematic = 0;

%% Compute CGM from Moveck

joints_name_kinematics = Param.kinematicsMapping(2:2:end);
joints_name_kinematics_KLab = Param.kinematicsMapping(1:2:end);
joints_name_kinetics = Param.kineticsMapping(1:2:end);
joints_name_kinetics_KLab = Param.kineticsMapping(2:2:end);
joint_center_labels = Param.jointCentersMapping(2:2:end);
joint_center_labels_KLab = Param.jointCentersMapping(1:2:end);
% (MFC) try and catch block
% (MFC) declaration of a cell array which will contain the static and/or
% dynamic files producing an error and the error catched
error = {};
% (MFC) declaration of counters for the errors index and for the static
% files index
idx_error = 1;
counter = 1;
% (MFC) declaration of a cell array which will contain the static files
% producing a warning because of missing calibration settings and the
% corresponding warning messages
all_warning_msg = {};
% (MFC) declaration of a cell array which will contain the static files and
% their corresponding calibration settings
all_calibration_settings = {};
try
    %% Select the dynamic files corresponding to a static file
    file.static = cellstr(Stat_files);
    idx_static = strfind(Stat_files, '\');
    sfile = Stat_files(idx_static(end)+1:end);
    idx_s = strfind(sfile, '-');
%     file.dynamic = {};
%     count = 0;
    file.dynamic = cellstr(Dyn_files);
%     for d = 1:length(Dyn_files)
%         idx_dynamic = strfind(Dyn_files{d}, '\');
%         dfile = Dyn_files{d}(idx_dynamic(end)+1:end);
%         idx_d = strfind(dfile, '-');
%         if isequal(extractBefore(dfile, '-'), extractBefore(sfile, '-')) && isequal(extractBetween(dfile, idx_d(1)+2, idx_d(2)-1), extractBetween(sfile, idx_s(1)+2, idx_s(2)-1))
%             count = count + 1;
%             file.dynamic{count} = Dyn_files{d};
%         end
%     end
    %% Displays
    disp(' *** K-LAB ANALYSIS START *** ');
    disp([' * number of static files ', num2str(length(file.static))]);
    disp([' * number of dynamic files ', num2str(length(file.dynamic))]);
    %% Kinematic and/or kinetic computation
    [O, store, calibration_parameters] = klab_cgm_compute(file, Param);
    % (MFC) filling all_warning_msg with the static files
    % producing a warning and their corresponding warning messages
    all_warning_msg = [all_warning_msg; warning_msg];
    warning_msg = {};
    % filling all_calibration_settings with the static files and
    % their corresponding calibration settings
    all_calibration_settings = [all_calibration_settings; calibration_settings];
    calibration_settings = {};
%     for o = 1:length(O)
        acq = btkReadAcquisition(O(1).trial_filenames);
%         % (MFC) clear all the points computed previously by another
%         % software in the acquisition of c3d files in order to have
%         % only the points computed with Moveck
%         btkClearPoints(acq);
%         %% Processed markers (MFC)
%         % (MFC) add new markers and virtual markers to c3d file
%         if Param.exportC3D_markers
%             for j = 1:size(O(o).markers, 1)
%                 v = O(o).markers{j, 1};
%                 VALUES = O(o).markers{j, 2};
%                 % (MFC) replace 0 by NaN values
%                 idx_zeros = find(VALUES == 0);
%                 if ~isempty(idx_zeros)
%                     VALUES(idx_zeros) = NaN;
%                 end
%                 btkAppendPoint(acq, 'marker', v, VALUES);
%             end
%         end
%         %% Joint centers (MFC)
%         if Param.exportC3D_jointCenters
%             for j = 1:length(joint_center_labels)
%                 if isfield(O(o).output, [joint_center_labels{j} 'Segments']) == 1
%                     v = joint_center_labels_KLab{j};
%                     VALUES = O(o).output.([joint_center_labels{j} 'Segments']);
%                     % (MFC) replace 0 by NaN values
%                     idx_zeros = find(VALUES == 0);
%                     if ~isempty(idx_zeros)
%                         VALUES(idx_zeros) = NaN;
%                     end
%                     if any(ismember(O(o).markers(:, 1), joint_center_labels_KLab{j}))
%                         btkSetPoint(acq, v, VALUES);
%                     else
%                         btkAppendPoint(acq, 'marker', v, VALUES);
%                     end
%                 end
%             end
%         end
        %% Kinematic new angles
        if Param.exportC3D_angles
            passekinematic = 0;
            point_names = fieldnames(btkGetPoints(acq));
            for j = 1:length(joints_name_kinematics)
                % Left angles
                if isfield(O(1).output, ['Left' joints_name_kinematics{j} 'Angles']) == 1
                    passekinematic = 1;
                    v = ['L' joints_name_kinematics_KLab{j} 'Angles'];
                    VALUES = O(1).output.(['Left' joints_name_kinematics{j} 'Angles']);
                    % (MFC) replace 0 by NaN values
                    idx_zeros = find(VALUES == 0);
                    if ~isempty(idx_zeros)
                        VALUES(idx_zeros) = NaN;
                    end
                    if any(strcmp(point_names, v))
                        btkSetPoint(acq, v, VALUES);
                    else
                        btkAppendPoint(acq, 'angle', v, VALUES);
                    end
                end
                % Right angles
                if isfield(O(1).output, ['Right' joints_name_kinematics{j} 'Angles']) == 1
                    passekinematic = 1;
                    v = ['R' joints_name_kinematics_KLab{j} 'Angles'];
                    VALUES = O(1).output.(['Right' joints_name_kinematics{j} 'Angles']);
                    % (MFC) replace 0 by NaN values
                    idx_zeros = find(VALUES == 0);
                    if ~isempty(idx_zeros)
                        VALUES(idx_zeros) = NaN;
                    end
                    if any(strcmp(point_names, v))
                        btkSetPoint(acq, v, VALUES);
                    else
                        btkAppendPoint(acq, 'angle', v, VALUES);
                    end
                end
            end
        end
%         %% Kinetic new moments and forces
%         if (Param.exportC3D_forces || Param.exportC3D_moments || Param.exportC3D_powers)
%             passekinetic = 0;
%             div = O(o).subject_details.SubjectMass_kg;
%             for j = 1:length(joints_name_kinetics)
%                 if Param.exportC3D_moments
%                     % Left moments
%                     if isfield(O(o).output, ['Left' joints_name_kinetics{j} 'Moment']) == 1
%                         passekinetic = 1;
%                         v = ['L' joints_name_kinetics_KLab{j} 'Moment'];
%                         VALUES = O(o).output.(['Left' joints_name_kinetics{j} 'Moment']);
%                         % (MFC) replace 0 by NaN values
%                         idx_zeros = find(VALUES == 0);
%                         if ~isempty(idx_zeros)
%                             VALUES(idx_zeros) = NaN;
%                         end
%                         btkAppendPoint(acq, 'moment', v, VALUES./div);
%                     end
%                     % Right moments
%                     if isfield(O(o).output,['Right' joints_name_kinetics{j} 'Moment']) == 1
%                         passekinetic = 1;
%                         v = ['R' joints_name_kinetics_KLab{j} 'Moment'];
%                         VALUES = O(o).output.(['Right' joints_name_kinetics{j} 'Moment']);
%                         % (MFC) replace 0 by NaN values
%                         idx_zeros = find(VALUES == 0);
%                         if ~isempty(idx_zeros)
%                             VALUES(idx_zeros) = NaN;
%                         end
%                         btkAppendPoint(acq, 'moment', v, VALUES./div);
%                     end
%                 end
%                 if Param.exportC3D_forces
%                     % Left forces
%                     if isfield(O(o).output,['Left' joints_name_kinetics{j} 'Force']) == 1
%                         passekinetic = 1;
%                         v = ['L' joints_name_kinetics_KLab{j} 'Force'];
%                         VALUES = O(o).output.(['Left' joints_name_kinetics{j} 'Force']);
%                         % (MFC) replace 0 by NaN values
%                         idx_zeros = find(VALUES == 0);
%                         if ~isempty(idx_zeros)
%                             VALUES(idx_zeros) = NaN;
%                         end
%                         btkAppendPoint(acq, 'force', v, VALUES./div);
%                     end
%                     % Right forces
%                     if isfield(O(o).output,['Right' joints_name_kinetics{j} 'Force']) == 1
%                         passekinetic = 1;
%                         v = ['R' joints_name_kinetics_KLab{j} 'Force'];
%                         VALUES = O(o).output.(['Right' joints_name_kinetics{j} 'Force']);
%                         % (MFC) replace 0 by NaN values
%                         idx_zeros = find(VALUES == 0);
%                         if ~isempty(idx_zeros)
%                             VALUES(idx_zeros) = NaN;
%                         end
%                         btkAppendPoint(acq, 'force', v, VALUES./div);
%                     end
%                 end
%             end
%         end
%         %% Write in metadata
%         % (MFC) remove all metadata in Field PROCESSING of c3d
%         % files (corresponding to processing information of other
%         % softwares) in order to have only the processing
%         % information of the Moveck software
%         md = btkFindMetaData(acq, 'PROCESSING');
%         if ~isnumeric(md)
%             btkRemoveMetaData(acq, 'PROCESSING');
%         end
%         % Add in field PROCESSING the subfields MODEL_SOFTWARE, MODEL_VERSION,
%         % Marker_filter_Processing, Marker_fillGap_Processing, MODEL_DATE_PROCESSING
%         % (MFC) add in field PROCESSING the calibration parameters
%         % (with default, measured or computed prefix)
%         if passekinematic
%             % add MODEL_SOFTWARE in PROCESSING
%             INFO = btkMetaDataInfo('Char', {'Moveck'});
%             btkAppendMetaData(acq, 'PROCESSING', 'MODEL_SOFTWARE', INFO);
%             % add MODEL_VERSION in PROCESSING
%             if passekinetic
%                 INFO = btkMetaDataInfo('Char', {'CGM1_0'});
%             else
%                 INFO = btkMetaDataInfo('Char', {'CGM1_0_OnlyKinematic'});
%             end
%             btkAppendMetaData(acq, 'PROCESSING', 'MODEL_VERSION', INFO);
%             % add MODEL_DATE_PROCESSING in PROCESSING
%             INFO = btkMetaDataInfo('Char', {datestr(now, 'yyyy-mm-dd')});
%             btkAppendMetaData(acq, 'PROCESSING', 'DATE_MODEL_PROCESSING', INFO);
%             % add Marker_filter_Processing in PROCESSING
%             INFO = btkMetaDataInfo('Char', {['Zero-lag Butterworth low-pass filter: cut-off=', ...
%                 num2str(Param.markerFilterCutoffFrequency), 'Hz, order=', ...
%                 num2str(Param.markerFilterOrder)]});
%             btkAppendMetaData(acq, 'PROCESSING', 'MARKER_FILTERING', INFO);
%             % add Marker_fillGap_Processing in PROCESSING
%             INFO = btkMetaDataInfo('Char', {['Fill Gap: max gap factor=', ...
%                 num2str(Param.markerFillgapMaxGapLength)]});
%             btkAppendMetaData(acq, 'PROCESSING', 'MARKER_FILLGAP', INFO);
%             % (MFC) add static file used for calibration
%             INFO = btkMetaDataInfo('Char', {sfile});
%             btkAppendMetaData(acq, 'PROCESSING', 'STATIC_FILE', INFO);
%             % (MFC) add information about the data computed by
%             % Moveck (1 if computed, 0 if not)
%             param_fields = fieldnames(Param);
%             export_fields = param_fields(contains(param_fields, 'exportC3D'));
%             export_fields = export_fields(~contains(export_fields, 'markers'));
%             computed_fields = replace(export_fields, 'exportC3D', 'Computation');
%             for i = 1:length(computed_fields)
%                 INFO = btkMetaDataInfo('Char', {num2str(Param.(export_fields{i}))});
%                 btkAppendMetaData(acq, 'PROCESSING', computed_fields{i}, INFO);
%             end
%             % (MFC) add calibration parameters in PROCESSING
%             % (with 3 possible prefix: default, measured or computed)
%             labels_calibration_parameters = fieldnames(calibration_parameters);
%             for i = 1:length(labels_calibration_parameters)
%                 INFO = btkMetaDataInfo('Char', {num2str(calibration_parameters(1).(labels_calibration_parameters{i}))});
%                 btkAppendMetaData(acq, 'PROCESSING', labels_calibration_parameters{i}, INFO);
%                 btkSetMetaDataDescription(acq, 'PROCESSING', labels_calibration_parameters{i}, calibration_parameters(2).(labels_calibration_parameters{i}));
%             end
%         end
        btkWriteAcquisition(acq, [O(1).trial_filenames(1:end-4) '.c3d']);
        btkDeleteAcquisition(acq);
%     end
catch ME
    % (MFC) catch static and/or dynamic file producing an error and the
    % error produced
    if ~isempty(error_file_idx{1})
        ks = strfind(file.static{error_file_idx{1}}, '\');
        error{idx_error, 1} = extractAfter(file.static{error_file_idx{1}}, ks(end));
    end
    if ~isempty(error_file_idx{2})
        if (error_file_idx{2} == 1)
            kd = strfind(file.static{error_file_idx{2}}, '\');
            error{idx_error, 2} = extractAfter(file.static{error_file_idx{2}}, kd(end));
        else
            kd = strfind(file.dynamic{error_file_idx{2}-1}, '\');
            error{idx_error, 2} = extractAfter(file.dynamic{error_file_idx{2}-1}, kd(end));
        end
    end
    error{idx_error, 3} = ME;
    idx_error = idx_error + 1;
    % (MFC) filling all_warning_msg with the static files producing a
    % warning and their corresponding warning messages
    all_warning_msg = [all_warning_msg; warning_msg];
    warning_msg = {};
    % filling all_calibration_settings with the static files and their
    % corresponding calibration settings
    all_calibration_settings = [all_calibration_settings; calibration_settings];
    calibration_settings = {};
end

%% Export files' name generating errors in xlsx file (MFC)
error_files = [];
error_message = [];
stack_table = [];
for i = 1:size(error, 1)
    column_spaces = cell((size(error{i, 3}.stack, 1) - 1), 2);
    error_files = [error_files; cell2table([{error{i, 1:2}}; column_spaces], 'VariableNames', {'static file', 'dynamic file'})];
    error_message = [error_message; cell2table([{error{i, 3}.identifier, error{i, 3}.message}; column_spaces], 'VariableNames', {'identifier', 'message'})];
    stack_table = [stack_table; struct2table(error{i, 3}.stack)];
end
table_error = [error_files, error_message, stack_table];
if ~isempty(table_error)
    writetable(table_error, 'table_error.xlsx');
end

%% Export static files' names generating warnings in xlsx file (MFC)
% warnings for static files for which some calibration settings are missing
% or have unexpected values
if ~isempty(all_warning_msg)
    table_warning = cell2table(all_warning_msg, 'VariableNames', {'static file' 'warning'});
    writetable(table_warning, 'table_warning.xlsx');
end

end

